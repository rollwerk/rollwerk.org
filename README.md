



# ROLLWERK Website

![](assets/img/Logo-Rollwerk.png)

## Development

Requirements:

- git ([how to install git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git))
- docker ([how to install docker](https://docs.docker.com/get-docker/))
- docker-compose ([how to install docker-compose](https://docs.docker.com/compose/install/))

To set up your environment to develop this website, clone this repo or your fork.

```sh
$ git clone https://gitlab.com/rollwerk/rollwerk.gitlab.io.git
$ cd rollwerk.gitlab.io
```

Then run:

```sh
$ docker-compose up -d
```

To check the logs, run this. (Using the `--follow` flag for tail the output.)

```sh
$ docker-compose logs --follow
```

Then open your browser at:

- http://localhost:4000

Add pages, documents, data, etc. like normal to test the website's contents. As you make modifications, your site will regenerate and you should see the changes in the browser after a refresh.

To stop the website:

```sh
$ docker-compose down
```



## Credits
### [Agency Jekyll Theme](https://github.com/raviriley/agency-jekyll-theme) Starter Template
[![RubyGems Downloads](https://img.shields.io/gem/dt/jekyll-agency.svg)](https://rubygems.org/gems/jekyll-agency)
[![LICENSE](https://img.shields.io/badge/license-MIT-lightgrey.svg)](https://github.com/raviriley/agency-jekyll-theme/blob/master/LICENSE.txt)
[![Tip Me via PayPal](https://img.shields.io/badge/PayPal-tip%20me-green.svg?logo=paypal)](https://www.paypal.me/raviriley)

This is the fastest and easiest way to get up and running on GitHub/Gitlab Pages.
Simply generate your own repository by clicking the [![template button](https://img.shields.io/badge/-Use%20this%20template-brightgreen)](https://github.com/raviriley/agency-jekyll-theme-starter/generate) button, 
then replace the sample content with your own and configure for your needs.

If you want your website to be at `YOUR-USERNAME.github.io` or `YOUR-USERNAME.gitlab.io`, that's what you must name your repo. If you want it to be like this: `https://raviriley.github.io/agency-jekyll-theme-starter/`, them make sure the `baseurl` in `_config.yml` matches the name of your repo. You can also use a `CNAME` file and your own custom domain!

**If you enjoy this theme, please consider [supporting me](https://www.paypal.me/raviriley) to continue developing and maintaining it.**

